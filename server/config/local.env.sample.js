'use strict';

// Use local.env.js for environment variables that will be set when the server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.

module.exports = {
  DOMAIN: 'http://localhost:9000',
  SESSION_SECRET: 'hunchapp-secret',

  GOOGLE_ID: '109285393983-l8kvaogdn1gg2bsmcf0huv9vurdpcba8.apps.googleusercontent.com',
  GOOGLE_SECRET: '0mk97Nw8e3wDgTZKnVBmincU',

  // Control debug level for modules using visionmedia/debug
  DEBUG: ''
};
