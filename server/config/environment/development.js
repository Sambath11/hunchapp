'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://Socrates:Plat0@ds035766.mlab.com:35766/platonideas'
  },

  // Seed database on startup
  seedDB: true

};
