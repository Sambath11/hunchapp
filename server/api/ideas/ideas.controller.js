/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/ideas              ->  index
 * POST    /api/ideas              ->  create
 * GET     /api/ideas/:id          ->  show
 * PUT     /api/ideas/:id          ->  upsert
 * PATCH   /api/ideas/:id          ->  patch
 * DELETE  /api/ideas/:id          ->  destroy
 */

'use strict';

import jsonpatch from 'fast-json-patch';
import Ideas from './ideas.model';
import Mails from './mail.model';
var nodemailer = require("nodemailer");
var mongoose = require("mongoose");
var paginate = require("mongoose-paginate");
var userschema = require("../user/user.model");

let newIdea = false;
let ideaUid = "";
let ideaD = "";
let ideaSD = "";
let ideaT = "";
let htmlbody = "";
let recieverList = "";
var mailOptions = "";

//Pagination through the npm package: "mongoose-paginate"


// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "aman@discoverdollar.com",
        pass: "Sangeeta@123"
    }
});


function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  if(newIdea)
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
        }else{
            console.log("Message sent: " + response.message);
        }
        // if you don't want to use this transport object anymore, uncomment following line
        newIdea = false;
        smtpTransport.close();
    });

  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}


export function iPaginate(req,res){
  Ideas.paginate({}, { page: req.params.page, limit: parseInt(req.params.limit,10) })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function sPaginate(req,res){
  Ideas.find().distinct("subDomain")
    .paginate({}, { page2: req.params.page, limit2: parseInt(req.params.limit,10) })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

export function count(req,res){
  return Ideas.count()
  .then(respondWithResult(res))
    .catch(handleError(res));
}

//API for like button
export function likes(req, res){
    Ideas.findById(req.body._id, function(err, collection){
        if(err){
            console.log(err);
        } else {
            collection.likes += 1;
            collection.save();
        }
        respondWithResult(likes);

    })
    .catch(err=>{
        console.error(err);
      });
}

// Gets a list of Ideass
export function index(req, res) {
  return Ideas.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function domlist(req,res){
  return Ideas.find().distinct("domain")
  .then(respondWithResult(res))
    .catch(handleError(res));
}

export function searchByDomain(req, res) {
  return Ideas.find({"domain":req.params.domain}).exec()
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

export function searchBySubDomain(req, res) {

  return Ideas.find({"subDomain":req.params.domain}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function searchByTitle(req, res) {
  return Ideas.find({"tittle":req.params.domain}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function searchByUser(req, res) {
  return Ideas.find({"uid":req.params.domain}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

export function subDomainlist(req,res){
  return Ideas.find().distinct("subDomain")
  .then(respondWithResult(res))
    .catch(handleError(res));
}

export function titlelist(req,res){
  return Ideas.find().distinct("tittle")
  .then(respondWithResult(res))
    .catch(handleError(res));
}

export function userlist(req,res){
  return Ideas.find().distinct("uid")
  .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets a list of Ideass by Domain
export function showByDomain(req, res) {
  return Ideas.find({"domain":req.params.domain}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Ideas by SubDomain
export function showBySubDomain(req, res) {
  return Ideas.find({"domain":req.params.domain, "subDomain" : req.params.subDomain}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}


// Gets a single Ideas from the DB
export function show(req, res) {
  return Ideas.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Ideas in the DB
export function create(req, res) {
  newIdea = true;
  ideaD = req.body.domain;
  ideaSD = req.body.subDomain;
  ideaT = req.body.tittle;
  ideaUid = req.session.user.name;
  recieverList = req.body.mail;
  let dt = new Date().toUTCString();
  console.log("Message sent to: ",recieverList);
  htmlbody = "<b> Domain :</b>"+ideaD + " <br><b>Idea SubDomain :</b> " + ideaSD + " <br><b>Idea Tittle : </b>" + ideaT + " <br><b>Added By :</b> " + ideaUid +"</br> <b>At :</b> " + dt + "<br> <h3>Thank You</h3>";
  mailOptions = {
        // sender address
        from: "aman@discoverdollar.com",
        // list of receivers
        to: recieverList,//["amanburman2609@gmail.com","sambath@discoverdollar.com"],
        subject: "New Idea Has been Added", // Subject line
        //text: "Domain : "+ideaD + " Idea SubDomain " + ideaSD + " Idea Tittle " + ideaT + " Added By " + ideaUid, // plaintext body
        html: htmlbody // html body
    }
  req.body.uid = req.session.user.name;
  return Ideas.create(req.body)
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}

//Creates a new comment and insert it into Ideas model
export function commenting(req, res) {
      let dt2 = new Date().toUTCString();
      var ideaId = req.body._id;
      Ideas.find({"_id":ideaId},(err,result)=>{
        let doc = result[0];
        doc.Comments.push(
          ideaId,
          {
            'cuid': req.session.user.name,
            'body': req.body.comment
          });
        doc.save();
        respondWithResult(doc);
    })
      .catch(err=>{
        console.error(err);
      });
}


// Upserts the given Ideas in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Ideas.findOneAndUpdate({_id: req.params.id}, req.body, {upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Ideas in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Ideas.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Ideas from the DB
export function destroy(req, res) {
  return Ideas.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
