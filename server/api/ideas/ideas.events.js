/**
 * Ideas model events
 */

'use strict';

import {EventEmitter} from 'events';
import Ideas from './ideas.model';
var IdeasEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
IdeasEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Ideas.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    IdeasEvents.emit(event + ':' + doc._id, doc);
    IdeasEvents.emit(event, doc);
  };
}

export default IdeasEvents;
