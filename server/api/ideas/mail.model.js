'use strict';

import mongoose from 'mongoose';

var MailSchema = new mongoose.Schema({
    email: {
            type: String,
            lowercase: true,
            required() {
            if(authTypes.indexOf(this.provider) === -1) {
                return true;
            } else {
                return false;
            }
            }
    }
});


export default mongoose.model('mails', MailSchema);
