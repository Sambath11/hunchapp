'use strict';

var express = require('express');
var controller = require('./ideas.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/count', controller.count);
router.get('/getDomLi', controller.domlist);
router.get('/subDomains', controller.subDomainlist);
router.get('/titles', controller.titlelist);
router.get('/users', controller.userlist);
router.get('/dom/:domain', controller.showByDomain);
router.get('/searchDom/:domain', controller.searchByDomain);
router.get('/searchSubDom/:domain', controller.searchBySubDomain);
router.get('/searchTitle/:domain', controller.searchByTitle);
router.get('/searchUser/:domain', controller.searchByUser);
router.get('/dom2/:domain/:subDomain', controller.showBySubDomain);
router.get('/paginate/:page/:limit', controller.iPaginate);
router.get('/paginate2/:page2/:limit2',controller.sPaginate);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/like', controller.likes);
router.post('/comments', controller.commenting);
router.post('/:id', controller.upsert);
router.patch('/:id', controller.patch);
router.delete('/:id', controller.destroy);

module.exports = router;
