'use strict';

import mongoose from 'mongoose';
var paginate = require('mongoose-paginate');


var IdeasSchema = new mongoose.Schema({
    //_id:{type : String, index:{ unique: true }},
    uid:String,
    domain:String,
    subDomain:String,
    tittle:String,
    ibody:String,
    status:String,
    likes: { type: Number, default: 0 },
    keywords: [],
    dateTime:{ type: Date, default: Date.now },
    Comments:[{cuid:String, date:{ type: Date, default: Date.now }, body:String}]
});

IdeasSchema.plugin(paginate);

//var Ideas = mongoose.model('Ideas',  schema); // Model.paginate()

export default mongoose.model('ideas', IdeasSchema);
