'use strict';

var app = require('../..');
import request from 'supertest';

var newIdeas;

describe('Ideas API:', function() {
  describe('GET /api/ideas', function() {
    var ideass;

    beforeEach(function(done) {
      request(app)
        .get('/api/ideas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          ideass = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(ideass).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/ideas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/ideas')
        .send({
          name: 'New Ideas',
          info: 'This is the brand new ideas!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newIdeas = res.body;
          done();
        });
    });

    it('should respond with the newly created ideas', function() {
      expect(newIdeas.name).to.equal('New Ideas');
      expect(newIdeas.info).to.equal('This is the brand new ideas!!!');
    });
  });

  describe('GET /api/ideas/:id', function() {
    var ideas;

    beforeEach(function(done) {
      request(app)
        .get(`/api/ideas/${newIdeas._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          ideas = res.body;
          done();
        });
    });

    afterEach(function() {
      ideas = {};
    });

    it('should respond with the requested ideas', function() {
      expect(ideas.name).to.equal('New Ideas');
      expect(ideas.info).to.equal('This is the brand new ideas!!!');
    });
  });

  describe('PUT /api/ideas/:id', function() {
    var updatedIdeas;

    beforeEach(function(done) {
      request(app)
        .put(`/api/ideas/${newIdeas._id}`)
        .send({
          name: 'Updated Ideas',
          info: 'This is the updated ideas!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedIdeas = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedIdeas = {};
    });

    it('should respond with the original ideas', function() {
      expect(updatedIdeas.name).to.equal('New Ideas');
      expect(updatedIdeas.info).to.equal('This is the brand new ideas!!!');
    });

    it('should respond with the updated ideas on a subsequent GET', function(done) {
      request(app)
        .get(`/api/ideas/${newIdeas._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let ideas = res.body;

          expect(ideas.name).to.equal('Updated Ideas');
          expect(ideas.info).to.equal('This is the updated ideas!!!');

          done();
        });
    });
  });

  describe('PATCH /api/ideas/:id', function() {
    var patchedIdeas;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/ideas/${newIdeas._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Ideas' },
          { op: 'replace', path: '/info', value: 'This is the patched ideas!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedIdeas = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedIdeas = {};
    });

    it('should respond with the patched ideas', function() {
      expect(patchedIdeas.name).to.equal('Patched Ideas');
      expect(patchedIdeas.info).to.equal('This is the patched ideas!!!');
    });
  });

  describe('DELETE /api/ideas/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/ideas/${newIdeas._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when ideas does not exist', function(done) {
      request(app)
        .delete(`/api/ideas/${newIdeas._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
