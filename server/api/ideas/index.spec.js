'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var ideasCtrlStub = {
  index: 'ideasCtrl.index',
  show: 'ideasCtrl.show',
  create: 'ideasCtrl.create',
  upsert: 'ideasCtrl.upsert',
  patch: 'ideasCtrl.patch',
  destroy: 'ideasCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var ideasIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './ideas.controller': ideasCtrlStub
});

describe('Ideas API Router:', function() {
  it('should return an express router instance', function() {
    expect(ideasIndex).to.equal(routerStub);
  });

  describe('GET /ideas', function() {
    it('should route to ideas.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'ideasCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /ideas/:id', function() {
    it('should route to ideas.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'ideasCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /ideas', function() {
    it('should route to ideas.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'ideasCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /ideas/:id', function() {
    it('should route to ideas.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'ideasCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /ideas/:id', function() {
    it('should route to ideas.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'ideasCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE ideas/:id', function() {
    it('should route to ideas.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'ideasCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
