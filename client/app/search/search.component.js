'use strict';

import angular from 'angular';
import routing from './search.routes';

/*@ngInject*/
export class searchController {
  constructor($http, $scope, socket, $stateParams, Auth, $state){

    'ngInject';

    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;

    $scope.isCollapsed = true;
    $scope.Search = $stateParams.Search;
    $http.get('/api/ideas/getDomLi').then(response =>{
      $scope.domlist = response.data;
    });
    $http.get('/api/ideas/subDomains').then(response =>{
      $scope.subDomlist = response.data;
    });
    $http.get('/api/ideas/titles').then(response =>{
      $scope.titles = response.data;
    });

    $http.get('/api/ideas/users').then(response =>{
      $scope.usrs = response.data;
    });
    $scope.viewby = 5;
    //$scope.totalItems = $scope.ideas.length;
    $scope.currentPage = 1;
    $scope.itemsPerPage = $scope.viewby;
    $scope.maxSize = 5; //Number of pager buttons to show

    $scope.Result = function(search){
      $http.get('/api/ideas/searchDom/'+search).then(response =>{
        $scope.ideas =response.data;
      });
      $http.get('/api/ideas/searchSubDom/'+search).then(response =>{
        for( var i = 0; i < response.data.length; i++){
    $scope.ideas.push(response.data[i]);
}
      });
      $http.get('/api/ideas/searchTitle/'+search).then(response =>{
        for( var i = 0; i < response.data.length; i++){
    $scope.ideas.push(response.data[i]);
}
      });
      $http.get('/api/ideas/searchUser/'+search).then(response =>{
        for( var i = 0; i < response.data.length; i++){
    $scope.ideas.push(response.data[i]);
}
      });

    };

    $scope.likeButton = function(id){
      for(var i=0;i<$scope.ideas.length;i++){
        if($scope.ideas[i]._id === id){
          $scope.ideas[i].likes+=1;
        }
      }
      $http.post('/api/ideas/like', JSON.stringify({ '_id' : id  })).then(response => {

      });
    };


    $scope.comments=function(id,comment,user){
      for(var i=0;i<$scope.ideas.length;i++){
        if($scope.ideas[i]._id === id)
        $scope.ideas[i].Comments.push({'cuid':user,'body':comment});
      }
      $http.post('/api/ideas/comments', JSON.stringify({ '_id' : id , 'comment' : comment }))
          .success(function(data, status) {

         });
    };




  $scope.setPage = function (pageNo,num) {
  $scope.currentPage = pageNo;
  $scope.itemsPerPage = num;
  };

  $scope.pageChanged = function() {
  console.log('Page changed to: ' + $scope.currentPage);
  };

  $scope.setItemsPerPage = function(num,pgno) {
  $scope.itemsPerPage = num;
  $scope.currentPage = pgno; //reset to first paghe
  };



    this.$http = $http;
    this.socket = socket;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('thing');
    });


  }



  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
        this.socket.syncUpdates('thing', this.awesomeThings);
      });
  }

  addThing() {
    if(this.newThing) {
      this.$http.post('/api/things', {
        name: this.newThing
      });
      this.newThing = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete(`/api/things/${thing._id}`);
  }



}

export default angular.module('hunchAppApp.search', ['ui.router', 'ngAnimate', 'ngSanitize', 'ui.bootstrap'])
.config(routing)
.component('search', {
  templateUrl: 'app/search/search.html',
  controller: searchController
})
  .name;
