'use strict';

describe('Controller: SearchCtrl', function() {
  // load the controller's module
  beforeEach(module('hunchAppApp.search'));

  var SearchCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    SearchCtrl = $controller('SearchCtrl', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
