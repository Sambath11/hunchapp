'use strict';

export default function routes($stateProvider) {
  'ngInject';


  $stateProvider.state('add', {
    url: '/add/:Add',
    template: '<add></add>',
    authenticate: true
  });
}
