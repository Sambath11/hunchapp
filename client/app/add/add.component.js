'use strict';
import angular from 'angular';
import routing from './add.routes';

/*@ngInject*/
export class addController{
  constructor($http, $scope, socket, $stateParams, Auth, $state){
    'ngInject';

    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  //  $scope.useremail=$scope.user.email;

    $scope.isCollapsed = true;

    $scope.add = $stateParams.add;

    this.$http = $http;
    this.socket = socket;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('thing');
    });

    $http.get('/api/users/userPassing').then(response => {
      $scope.users=response.data;
    });

    $scope.subDomainlist = function(domain){
      $scope.dom=domain;
      $http.get('/api/ideas/dom/'+$scope.dom).then(response => {
        $scope.subDomains=response.data;
      });
    };

    $http.get('/api/ideas/getDomLi').then(response => {
      $scope.domains = response.data;
    });



    $scope.submit = function(ids,mail){
      console.log(mail);
      $scope.mail=[];
      if(mail[0] ==='All'){
        for(var i=0;i<$scope.users.length;i++){

          $scope.emailAdrs = $scope.users[i];
          if($scope.emailAdrs.name !== 'admin')
          $scope.mail.push($scope.emailAdrs.email);
        }
      }
      else{
        $scope.mail = mail;
      }
      console.log($scope.mail);
      $scope.uid=ids.name;
      $scope.date = new Date();
      $scope.status = 'active';
     $http.post('/api/ideas/', JSON.stringify({'domain': $scope.ids.domain  ,
           'subDomain': $scope.ids.subDomain , 'uid': $scope.ids.name , 'tittle': $scope.ids.tittle ,
            'ibody': $scope.ids.ibody , 'status': $scope.status , 'DateTime': $scope.date , 'mail' : $scope.mail }))
            .success(function(data, status) {
               $state.go('main');
           });

    };

  }



  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
        this.socket.syncUpdates('thing', this.awesomeThings);
      });
  }

  addThing() {
    if(this.newThing) {
      this.$http.post('/api/things', {
        name: this.newThing
      });
      this.newThing = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete(`/api/things/${thing._id}`);
  }
}

export default angular.module('hunchAppApp.add', ['ui.router', 'ngAnimate', 'ngSanitize', 'ui.bootstrap'])
.config(routing)
.filter('unique', function() {
 return function(collection, keyname) {
    var output = [],
        keys = [];

    angular.forEach(collection, function(item) {
        var key = item[keyname];
        if(keys.indexOf(key) === -1) {
            keys.push(key);
            output.push(item);
        }
    });

    return output;
 };
})

.component('add', {
  templateUrl: 'app/add/add.html',
  controller: addController
})
  .name;
