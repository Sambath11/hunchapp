'use strict';

describe('Controller: AddCtrl', function() {
  // load the controller's module
  beforeEach(module('hunchAppApp.add'));

  var AddCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    AddCtrl = $controller('AddCtrl', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
