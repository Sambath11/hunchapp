'use strict';
const angular = require('angular');

export class dateSearchComponent {
  /*@ngInject*/
  constructor() {
    this.message = 'World';
  }
}

export default angular.module('hunchAppApp.dateSearch', [])
  .component('dateSearch', {
    template: '<h1>Hello {{ $ctrl.message }}</h1>',
    bindings: { message: '<' },
    controller: dateSearchComponent
  })
  .name;
