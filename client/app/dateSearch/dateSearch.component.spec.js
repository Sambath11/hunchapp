'use strict';

describe('Component: dateSearch', function() {
  // load the component's module
  beforeEach(module('hunchAppApp.dateSearch'));

  var dateSearchComponent;

  // Initialize the component and a mock scope
  beforeEach(inject(function($componentController) {
    dateSearchComponent = $componentController('dateSearch', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
