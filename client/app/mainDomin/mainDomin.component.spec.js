'use strict';

describe('Component: mainDomin', function() {
  // load the component's module
  beforeEach(module('hunchAppApp.mainDomin'));

  var mainDominComponent;

  // Initialize the component and a mock scope
  beforeEach(inject(function($componentController) {
    mainDominComponent = $componentController('mainDomin', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
