import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './mainDomin.routes';
import ngAnimate from 'angular-animate';


export class mainDominComponent {
  /*@ngInject*/
  constructor($scope, $stateParams, $http, socket, Auth) {

    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;


    $scope.domainName=$stateParams.domainName;
    console.log($scope.domainName);
    $scope.domains=[{
  id: 1,
  name: 'Data Analyse'
  },{
  id: 2,
  name: 'Yeoman'
  },{
  id: 3,
  name: 'Python'
  }];
$scope.subDomains=[{
  id: 1,
  domainId: 1,
  name: 'Marketing'
  },{
  id: 2,
  domainId: 1,
  name: 'Business'
  },{
  id: 3,
  domainId: 2,
  name: 'Angular-Fullstack'
  },{
  id: 4,
  domainId: 2,
  name: 'Database'
  },{
  id: 5,
  domainId: 3,
  name: 'Connectivity'
  },{
  id: 6,
  domainId: 3,
  name: 'differences'
  }];
  $scope.ideas=[{
    id: 1,
    domainId: 1,
    SubDomainId: 1,
    title: 'aaa',
    content: 'AFeghGXGFGMF'
    },{
    id: 2,
    domainId: 1,
    SubDomainId: 1,
    title: 'BBB',
    content: 'AFeghGXGFGMF'
    },{
    id: 3,
    domainId: 1,
    SubDomainId: 2,
    title: 'CCC',
    content: 'AFeghGXGFGMF'
    },{
    id: 4,
    domainId: 1,
    SubDomainId: 2,
    title: 'DDD',
    content: 'AFeghGXGFGMF'
    },{
    id: 5,
    domainId: 2,
    SubDomainId: 1,
    title: 'EEE',
    content: 'AFeghGXGFGMF'
    },{
    id: 6,
    domainId: 2,
    SubDomainId: 1,
    title: 'FFF',
    content: 'AFeghGXGFGMF'
    },{
    id: 7,
    domainId: 2,
    SubDomainId: 2,
    title: 'ggg',
    content: 'AFeghGXGFGMF'
    },{
    id: 8,
    domainId: 2,
    SubDomainId: 2,
    title: 'hHh',
    content: 'AFeghGXGFGMF'
    },{
    id: 9,
    domainId: 3,
    SubDomainId: 1,
    title: 'aaa',
    content: 'AFeghGXGFGMF'
    },{
    id: 10,
    domainId: 3,
    SubDomainId: 1,
    title: 'aaa',
    content: 'AFeghGXGFGMF'
    },{
    id: 11,
    domainId: 3,
    SubDomainId: 2,
    title: 'aaa',
    content: 'AFeghGXGFGMF'
    },{
    id: 12,
    domainId: 3,
    SubDomainId: 2,
    title: 'aaa',
    content: 'AFeghGXGFGMF'
    }];

    this.$http = $http;
    this.socket = socket;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('thing');
    });

  }

  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
        this.socket.syncUpdates('thing', this.awesomeThings);
      });
  }

  addThing() {
    if(this.newThing) {
      this.$http.post('/api/things', {
        name: this.newThing
      });
      this.newThing = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete(`/api/things/${thing._id}`);
  }
}



  export default angular.module('hunchAppApp.mainDomin', ['ui.router', 'ui.bootstrap'])
  .config(routing)
  .component('mainDomin', {
    templateUrl: 'app/mainDomin/mainDomin.html',
    controller: mainDominComponent
  })
    .name
