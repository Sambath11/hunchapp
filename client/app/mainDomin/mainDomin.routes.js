'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('mainDomin', {
    url: '/mainDomin/:domainId',
    template: require('./mainDomin.html'),
    authenticate: true
  });
}
