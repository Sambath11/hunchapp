'use strict';

export default function routes($stateProvider) {
  'ngInject';

  $stateProvider.state('main', {
    url: '/main',
    template: '<main></main>',
    authenticate: true
  });
  $stateProvider.state('mains', {
    url: '/main/:domainName',
    template: '<main></main>',
    authenticate: true
  });
  $stateProvider.state('mainss', {
    url: '/main/:domainId/:SubDomainId',
    template: '<main></main>',
    authenticate: true
  });
  $stateProvider.state('mainS', {
    url: 'main/:Search',
    template: '<main></main>',
    authenticate: true
  });
  $stateProvider.state('mainAdd', {
    url: 'main/:add',
    template: '<main></main>',
    authenticate: true
  });
}
