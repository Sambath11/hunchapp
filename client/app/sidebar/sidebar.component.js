
import angular from 'angular';

export class sidebarComponent {

  constructor($http, $scope, Auth, socket) {
      'ngInject';

          $http.get('/api/ideas/getDomLi').then(response => {
            $scope.totalItems = response.data.length;
            $scope.domainList = response.data;
          });

            $scope.isCollapsed = true;
          $scope.viewby = 4;
          $scope.currentPage = 1;
          $scope.itemsPerPage = $scope.viewby;
          $scope.maxSize = 5; //Number of pager buttons to show


  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.pageChanged = function() {
    console.log('Page changed to: ' + $scope.currentPage);
  };

$scope.setItemsPerPage = function(num) {
  $scope.itemsPerPage = num;
  $scope.currentPage = 1; //reset to first paghe
};



this.isLoggedIn = Auth.isLoggedInSync;
this.isAdmin = Auth.isAdminSync;
this.getCurrentUser = Auth.getCurrentUserSync;

this.$http = $http;
this.socket = socket;

$scope.$on('$destroy', function() {
  socket.unsyncUpdates('thing');
});




}


  $onInit() {
this.$http.get('/api/things')
  .then(response => {
    this.awesomeThings = response.data;
    this.socket.syncUpdates('thing', this.awesomeThings);

  });
}

addThing() {
if(this.newThing) {
  this.$http.post('/api/things', {
    name: this.newThing
  });
  this.newThing = '';
}
}

deleteThing(thing) {
this.$http.delete(`/api/things/${thing._id}`);
}

}

export default angular.module('directives.sidebar', [ 'ui.router', 'ngAnimate', 'ngSanitize', 'ui.bootstrap'])
.filter('unique', function() {
 return function(collection, keyname) {
    var output = [],
        keys = [];

    angular.forEach(collection, function(item) {
        var key = item[keyname];
        if(keys.indexOf(key) === -1) {
            keys.push(key);
            output.push(item);
        }
    });

    return output;
 };
})
  .component('sidebar', {
    template: require('./sidebar.html'),
    controller: sidebarComponent
  })
  .name;
