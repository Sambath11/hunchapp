'use strict';
import angular from 'angular';

export class headerComponent {
  /*@ngInject*/
  constructor(Auth) {
    'ngInject';

    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;
  }
}

export default angular.module('directives.header', [])
  .component('header', {
    template: require('./header.html'),
    controller: headerComponent
  })
  .name;
