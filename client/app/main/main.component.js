  import angular from 'angular';
  //import uiRouter from 'angular-ui-router';
  import routing from './main.routes';
  //import ngAnimate from 'angular-animate';

  export class MainController {

    /*@ngInject*/
    constructor($http, $scope, $stateParams, $state, Auth, socket){
      'ngInject';


  $scope.isCollapsed = true;

  $scope.currentPage = 1;
  $scope.itemsPerPage = 5;
  $scope.maxSize = 5;

    $http.get('/api/ideas/paginate/'+$scope.currentPage+'/'+$scope.itemsPerPage).then(response => {
      $scope.sample = response.data;
      $scope.totalItems = response.data.total;
      $scope.ideas = $scope.sample.docs;
      $scope.limit = $scope.sample.limit;
      $scope.currentPage = $scope.sample.page;
      $scope.numPages = $scope.sample.pages;
    });

    $scope.likeButton = function(id){
      for(var i=0;i<$scope.ideas.length;i++){
        if($scope.ideas[i]._id === id){
          $scope.ideas[i].likes+=1;
        }
      }
      $http.post('/api/ideas/like', JSON.stringify({ '_id' : id  })).then(response => {

      });
    };

  $scope.setPage = function (pageNo,num) {
    $scope.currentPage = pageNo;
    $scope.itemsPerPage = num;

    $http.get('/api/ideas/paginate/'+$scope.currentPage+'/'+$scope.itemsPerPage).then(response => {
      $scope.sample = response.data;
      $scope.totalItems = response.data.total;
      $scope.ideas = $scope.sample.docs;
      $scope.limit = $scope.sample.limit;
      $scope.currentPage = $scope.sample.page;
      $scope.numPages = $scope.sample.pages;
      console.log($scope.ideas);

    });
  };

  $scope.pageChanged = function() {
    console.log('Page changed to: ' + $scope.currentPage);
  };

$scope.setItemsPerPage = function(num,pgno) {
  $scope.itemsPerPage = num;
  $scope.currentPage = pgno; //reset to first paghe
  $http.get('/api/ideas/paginate/'+$scope.currentPage+'/'+$scope.itemsPerPage).then(response => {
    $scope.sample = response.data;
    $scope.totalItems = response.data.total;
    $scope.ideas = $scope.sample.docs;
    $scope.limit = $scope.sample.limit;
    $scope.currentPage = $scope.sample.page;
    $scope.numPages = $scope.sample.pages;

  });

};

$scope.comments=function(id,comment,user){
  for(var i=0;i<$scope.ideas.length;i++){
    if($scope.ideas[i]._id === id)
    $scope.ideas[i].Comments.push({'cuid':user,'body':comment});
  }
  $http.post('/api/ideas/comments', JSON.stringify({ '_id' : id , 'comment' : comment }))
      .success(function(data, status) {
        $http.get('/api/ideas/paginate/'+$scope.currentPage+'/'+$scope.itemsPerPage).then(response => {
          $scope.sample = response.data;
          $scope.totalItems = response.data.total;
          $scope.ideas = $scope.sample.docs;
          $scope.limit = $scope.sample.limit;
          $scope.numPages = $scope.sample.pages;

        });
     });
};

this.isLoggedIn = Auth.isLoggedInSync;
this.isAdmin = Auth.isAdminSync;
this.getCurrentUser = Auth.getCurrentUserSync;

this.$http = $http;
this.socket = socket;

$scope.$on('$destroy', function() {
  socket.unsyncUpdates('thing');
});

}



  $onInit() {
this.$http.get('/api/things')
  .then(response => {
    this.awesomeThings = response.data;
    this.socket.syncUpdates('thing', this.awesomeThings);

  });
}

addThing() {
if(this.newThing) {
  this.$http.post('/api/things', {
    name: this.newThing
  });
  this.newThing = '';
}
}

deleteThing(thing) {
this.$http.delete(`/api/things/${thing._id}`);
}


}

export default angular.module('hunchAppApp.main', ['ui.router', 'ngAnimate', 'ngSanitize', 'ui.bootstrap'])
    .config(routing)
    .filter('unique', function() {
     return function(collection, keyname) {
        var output = [],
            keys = [];

        angular.forEach(collection, function(item) {
            var key = item[keyname];
            if(keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
     };
  })

    .component('main', {
      templateUrl: 'app/main/main.html',
      controller: MainController
    })
    .name;
