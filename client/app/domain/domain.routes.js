'use strict';

export default function routes($stateProvider) {
  'ngInject';


  $stateProvider.state('domain', {
    url: '/domain/:domainName',
    template: '<domain></domain>',
    authenticate: true
  });


}
