'use strict';

describe('Controller: DomainCtrl', function() {
  // load the controller's module
  beforeEach(module('hunchAppApp.domain'));

  var DomainCtrl;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($controller) {
    DomainCtrl = $controller('DomainCtrl', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
