'use strict';
import angular from 'angular';
import routing from './domain.routes';

/*@ngInject*/
export class domainController {
  constructor($http, $scope, socket, $stateParams, Auth, $state){

    'ngInject';

    this.isLoggedIn = Auth.isLoggedInSync;
    this.isAdmin = Auth.isAdminSync;
    this.getCurrentUser = Auth.getCurrentUserSync;;
  //  let $scope.name=auth.getCurrentUser();
  //console.log($scope.name);
  //  console.log(Auth.getCurrentUser().name);
    $scope.domainName = $stateParams.domainName;
    $scope.isCollapsed = true;
    $scope.currentPage = 1;
    $scope.itemsPerPage = 5
    $scope.maxSize = 5; //Number of pager buttons to show

  $http.get('/api/ideas/dom/'+$scope.domainName).then(response => {
    $scope.ideas = response.data;
    $scope.totalItems = $scope.ideas.length;

  });

  $scope.likeButton = function(id){
    for(var i=0;i<$scope.ideas.length;i++){
      if($scope.ideas[i]._id === id){
        $scope.ideas[i].likes+=1;
      }
    }
    $http.post('/api/ideas/like', JSON.stringify({ '_id' : id  })).then(response => {

    });
  };

  $scope.comments=function(id,comment,userName){
    for(var i=0;i<$scope.ideas.length;i++){
      if($scope.ideas[i]._id === id)
      $scope.ideas[i].Comments.push({'cuid':userName,'body':comment});
    }
    $http.post('/api/ideas/comments', JSON.stringify({ '_id' : id , 'comment' : comment }))
        .success(function(data, status) {
          $http.get('/api/ideas/dom/'+$scope.domainName).then(response => {
            $scope.ideas = response.data;
            $scope.totalItems = $scope.ideas.length;
          });
       });
  };




$scope.setPage = function (pageNo,num) {
$scope.currentPage = pageNo;
$scope.itemsPerPage = num;
};

$scope.pageChanged = function() {
console.log('Page changed to: ' + $scope.currentPage);
};

$scope.setItemsPerPage = function(num,pgno) {
$scope.itemsPerPage = num;
$scope.currentPage = pgno; //reset to first paghe
};


    this.$http = $http;
    this.socket = socket;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('thing');
    });
}



  $onInit() {
    this.$http.get('/api/things')
      .then(response => {
        this.awesomeThings = response.data;
        this.socket.syncUpdates('thing', this.awesomeThings);
      });
  }

  addThing() {
    if(this.newThing) {
      this.$http.post('/api/things', {
        name: this.newThing
      });
      this.newThing = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete(`/api/things/${thing._id}`);
  }
}

export default angular.module('hunchAppApp.domain', ['ui.router', 'ngAnimate', 'ngSanitize', 'ui.bootstrap'])
.config(routing)
.component('domain', {
  templateUrl: 'app/domain/domain.html',
  controller: domainController
})
  .name;
